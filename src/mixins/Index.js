/* eslint-disable no-console */
const mixin = {
  methods: {
    sortByKey(array, key) {
      if (!array) return false;
      return array.sort(function (a, b) {
        let x = a[key]; let y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
      });
    }

  }
};

export default mixin;