/* eslint-disable no-console */
import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

const ApiUrl = 'https://api.myjson.com';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    categories: null
  },

  getters: {
    categories: state => { return state.categories }
  },

  mutations: {
    SET_CATEGORIES(state, payload) {
      state.categories = payload.data;
    }
  },

  actions: {
    getCategories({ commit }) {
      axios.get(ApiUrl + '/bins/a71rx').then(response => {
        commit('SET_CATEGORIES', response);
      });
    }
  }

})
